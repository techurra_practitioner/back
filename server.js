//Preparamos la aplicación para poder documentar la API vía swagger
var swaggerJSDoc = require('swagger-jsdoc');

// swagger definition
var swaggerDefinition = {
  info: {
    title: 'techurra API',
    version: '0.8.0',
    description: 'API del proyecto del curso Tech U Practitioner Bootcamp 2018',
  },
  host: 'localhost:3000',
  basePath: '/',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./server.js'],
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

// Preparamos el entorno para Lanzar la API a la escucha en el puerto 3000
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

// Preparamos el entorno swaggerUI para poder mostrar la documentación de la API desde el navegador
var swaggerUi = require('swagger-ui-express');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//variable para poder usar el request-json
var requestJson = require('request-json');

//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
var path = require('path');

// Preparamos las variables para realizar la conexión a la base de datos
// urlusuariosMlab para los clientes, urlmovimientosMlab para obtener los movimientos
var urlusuariosMlab = "https://api.mlab.com/api/1/databases/techurra/collections/usuarios";
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/techurra/collections/cuentas";
var apiKey = "apiKey=BNj1Ur81e8K6Q8HAXb4tZ5WcXjW5N6Fa";
var clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + apiKey);

console.log("Bienvenido a la API del proyecto del curso Tech U Practitioner Bootcamp 2018.")
console.log("Versión 0.8.0")

// Lanzamos la API a la escucha en el puerto 3000
app.listen(port);
console.log('RESTful API server started on: ' + port);

// Dirigimos la conexión al raiz / hacia lá página /index.html
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

// Documentación de los definitions de swagger
/**
 * @swagger
 * definitions:
 *   usuario:
 *     properties:
 *       email:
 *         type: string
 *         example: estaples0@mysql.com
 *       password:
 *         type: string
 *         example: wtDPtoRefVq
 *   datoslogin:
 *     properties:
 *       login:
 *         type: string
 *         example: ok
 *       idcliente:
 *         type: integer
 *         example: 1
 *       nombre:
 *         type: string
 *         example: Emelina
 *       apellido:
 *         type: string
 *         example: Staples
 *   idcliente:
 *     properties:
 *       idcliente:
 *         type: integer
 *         example: 1
 *   datoslogout:
 *     properties:
 *       logout:
 *         type: string
 *         example: ok
 *       idcliente:
 *         type: integer
 *         example: 1
 *   datoscliente:
 *     properties:
 *       _id:
 *         type: object
 *         properties:
 *           $oid:
 *             type: string
 *             example: 5a9f1e9ad74ffe54fc85fd6d
 *       idcliente:
 *         type: integer
 *         example: 1
 *       nombre:
 *         type: string
 *         example: Emelina
 *       apellido:
 *         type: string
 *         example: Staples
 *       email:
 *         type: string
 *         example: estaples0@mysql.com
 *       password:
 *         type: password
 *         example: wtDPtoRefVq
 *       ciudad:
 *         type: string
 *         example: Bāglung
 *       pais:
 *         type: string
 *         example: NP
 *       direccion:
 *         type: string
 *         example: 06 Jana Avenue
 *       logged:
 *         type: boolean
 *         example: false
 *         required: false
 *   ibanescliente:
 *     items:
 *       type: object
 *       properties:
 *         iban:
 *           type: string
 *           example: NL51 TKMS 6419 1963 22
 *   movimientoscuenta:
 *     properties:
 *       movimientos:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *               example: 1
 *             fecha:
 *               type: string
 *               example: 01/01/2018
 *             importe:
 *               type: string
 *               example: -1167.32
 *             saldo:
 *               type: string
 *               example: 8832.68
 *             tipo:
 *               type: string
 *               example: REINTEGRO
 *   datosusuarionuevo:
 *     properties:
 *       nombre:
 *         type: string
 *         example: Belen
 *       apellido:
 *         type: string
 *         example: Martín de Bustamante
 *       email:
 *         type: string
 *         example: belenuca@gmail.com
 *       password:
 *         type: password
 *         example: 1234567890
 *       ciudad:
 *         type: string
 *         example: Madrid
 *       pais:
 *         type: string
 *         example: España
 *       direccion:
 *         type: string
 *         example: Balandro, 12
 *   datosusuarioexistente:
 *     properties:
 *       idcliente:
 *         type: Number
 *         example: 1001
 *       nombre:
 *         type: string
 *         example: Belen
 *       apellido:
 *         type: string
 *         example: Martín de Bustamante
 *       email:
 *         type: string
 *         example: belenuca@gmail.com
 *       password:
 *         type: password
 *         example: 1234567890
 *       ciudad:
 *         type: string
 *         example: Madrid
 *       pais:
 *         type: string
 *         example: España
 *       direccion:
 *         type: string
 *         example: Balandro, 12
 *   datosmovimiento:
 *     properties:
 *       iban:
 *         type: string
 *         example: PK66 YWBC 8PMY LTMJ LIQY RF3L
 *       fecha:
 *         type: string
 *         example: 01/03/2018
 *       imnporte:
 *         type: string
 *         example: -348
 *       tipo:
 *         type: string
 *         example: RECIBO
 *   saldo:
 *     properties:
 *       saldo:
 *         type: integer
 *         example: 10635.87
 *   datospassword:
 *     properties:
 *       idcliente:
 *         type: integer
 *         example: 1001
 *       oldPassword:
 *         type: password
 *         example: 1234567890
 *       newPassword:
 *         type: password
 *         example: 0987654321
 *   graficos:
 *     properties:
 *       ingresos:
 *         type: number
 *         example: 3547.12
 *       gastos:
 *         type: integer
 *         example: 2145.45
 *       saldos:
 *         type: array
 *         items:
 *           type: Number
 *         example: [128.53,-327.28,3500.0,-1200.0,35.12,-56.45,-34.24,-12.14]
 *   graficosv2:
 *     properties:
 *       piechart:
 *         type: array
 *         items:
 *           type: array
 *           items:
 *             oneOf:
 *               - type: String
 *               - type: Number
 *         example: [["ingresos",3935.63],["gastos",3299.76]]
 *       saldos:
 *         type: array
 *         items:
 *           type: array
 *           items:
 *             oneOf:
 *               - type: String
 *               - type: Number
 *         example: [["Fecha","Saldo"],["01/01/2018",8832.68],["01/02/2018",8575.71],["01/03/2018",9365.05],["01/04/2018",9081.69],["01/05/2018",10330.11],["01/06/2018",11748.14],["01/07/2018",11572.11],["01/08/2018",12051.95],["01/09/2018",10635.87]]
 */

/**
 * @swagger
 * /api:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada simple para probar si la API está a la escucha
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Un json con un mensaje de bienvenida
 */
app.get('/api', function(req, res){
res.send({"mensaje":"Bienvenido a la API del proyecto Tech-U Practitioner Bootcamp del Red Ribon Army. v0.8.0"})
})

// Añadimos la ruta para servir el wagger specserve swagger
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

/**
 * @swagger
 * /api/login:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para realizar el inicio de sesión en la aplicación bancaria.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: usuario
 *         description: Objeto de tipo usuario (cliente)
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/usuario'
 *     responses:
 *       200:
 *         description: Un json con los datos del cliente que ha iniciado sesión
 *         schema:
 *           $ref: '#/definitions/datoslogin'
 *       400:
 *         description: Un mensaje indicando que el usuario ya tiene una sesión iniciada.
 *       404:
 *         description: Un mensaje indicando que el usuario no existe
 */
app.post('/api/login', function(req, res){
  var user = req.body
  var email = user.email
  var password = user.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        console.log("logged = " + body[0].logged)
        if ((body[0].logged == undefined) || (body[0].logged == false))
        {
          clienteMlab = requestJson.createClient(urlusuariosMlab)
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"idcliente": ' + body[0].idcliente + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
          var datos = {}
          datos.login = "ok"
          datos.idcliente = body[0].idcliente
          datos.nombre = body[0].nombre
          datos.apellido = body[0].apellido
          res.send(datos)
          })
        }
        else {
          res.status(400).send('El usuario ya tiene una sesión iniciada.')
        }
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

/**
 * @swagger
 * /api/logout:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para realizar la desconexión del cliente de la aplicacion.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cliente
 *         description: Objeto de tipo idcliente
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/idcliente'
 *     responses:
 *       200:
 *         description: Un json con los datos del cliente que ha iniciado sesión
 *         schema:
 *           $ref: '#/definitions/datoslogout'
 *       404:
 *         description: Un mensaje indicando que el usuario no existe
 */
app.post('/api/logout', function(req, res){
var body = req.body
var idcliente = body.idcliente
var query = 'q={"idcliente":' + idcliente + ', "logged":true}'
clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + query + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // El cliente tenía la sesión establecida. Lo desconectamos
      clienteMlab = requestJson.createClient(urlusuariosMlab)
      var cambio = '{"$set":{"logged":false}}'
      clienteMlab.put('?q={"idcliente": ' + body[0].idcliente + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
        var datos = {}
        datos.logout = "ok"
        datos.idcliente = idcliente
        res.send(datos)
     })
    }
    else {
      res.status(200).send('No se ha realizado la desconexión porque el cliente no tenía una sesión establecida en la aplicación.')
    }
  }
})
})

/**
 * @swagger
 * /api/movimientos:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada que devuelve todos los movimientos de todos los clientes
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Un json con todos los movimientos de todos los clientes
 */
app.get('/api/movimientos', function(req, res) {
  var url = urlmovimientosMlab
  var limit = "l=2000"
  url += "?" + limit + "&" + apiKey
  clienteMlab = requestJson.createClient(url)

  clienteMlab.get('', function(err, resM, body) {
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  });
});

/**
 * @swagger
 * /api/clientes:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada que devuelve todos los datos de todos los clientes que hay en la base de datos
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Un json con todos los movimientos de todos los clientes
 */
app.get('/api/clientes', function(req, res) {
  var url = urlusuariosMlab
  var filter = "f={'idcliente':1, 'nombre': 1, 'apellido': 1}"
  var limit = "l=2000"
  url += "?" + filter + "&" + limit + "&" + apiKey
  clienteMlab = requestJson.createClient(url)

    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

/**
 * @swagger
 * /api/cliente/{idcliente}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada que devuelve todos los datos de un cliente identificado por su {idcliente}
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idcliente
 *         description: número entero que identifica de forma única a un cliente
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       200:
 *         description: Un json con todos los datos del un cliente
 *         schema:
 *           $ref: '#/definitions/datoscliente'
 *       404:
 *         description: Mensaje indicando que el cliente no existe
 */
app.get('/api/cliente/:idcliente', function(req, res){
  var idcliente = req.params.idcliente
  var query = 'q={"idcliente":' + idcliente + '}'
  clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay clientes
        res.send(body[0])
      }
      else {
        res.status(404).send('No existe ese cliente.')
      }
    }
  })
})

/**
 * @swagger
 * /api/cuentas/{idcliente}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada que devuelve todas las cuentas de un cliente identificado por su {idcliente}
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idcliente
 *         description: número entero que identifica de forma única a un cliente
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 808
 *     responses:
 *       200:
 *         description: Un array de objetos con los ibanes del cliente
 *         schema:
 *           $ref: '#/definitions/ibanescliente'
 *       404:
 *         description: Mensaje indicando que el cliente no existe o no tiene cuentas
 */
app.get('/api/cuentas/:idcliente', function(req, res){
  var idcliente = req.params.idcliente
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"iban":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay clientes
        res.send(body)
      }
      else {
        res.status(404).send('No existe ese cliente o no tiene cuentas.')
      }
    }
  })
})

app.get('/api/cuentasysaldos/:idcliente', function(req, res){
  var idcliente = req.params.idcliente
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"iban":1,"movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay clientes
        var arraycuentasaldos = []
        var cuentasaldo = {}
        var movimientos = []
        for (var i = 0; i < body.length; i++) {
          var cuentasaldo = {}
          cuentasaldo.iban = body[i].iban
          movimientos = body[i].movimientos
          cuentasaldo.saldo = movimientos[movimientos.length-1].saldo
          arraycuentasaldos.push(cuentasaldo)
        }
        res.send(arraycuentasaldos)
      }
      else {
        res.status(404).send('No existe ese cliente o no tiene cuentas.')
      }
    }
  })
})

/**
 * @swagger
 * /api/movimientos/{iban}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada para obtener todos los movimientos de una cuenta identificada por su {iban}
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: iban
 *         description: iban de la cuenta deee la que queremos saber los movimientos
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *         example: NL51 TKMS 6419 1963 22
 *     responses:
 *       200:
 *         description: Un objeto con un array de movimientos de la cuenta
 *         schema:
 *           $ref: '#/definitions/movimientoscuenta'
 *       404:
 *         description: Mensaje indicando que no existe esa cuenta.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.get('/api/movimientos/:iban', function(req, res){
  var iban = req.params.iban
  var query = 'q={"iban":"' + iban + '"}'
  var filter = 'f={"movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay cuentas
        // res.send(body)
        res.send(body[0].movimientos)
      }
      else {
        res.status(404).send('No existe ningún cliente con ese número de cuenta.')
      }
    }
  })
})

// Función que devuelte el idcliente a asignar a un cliente nuevo
// Busca el idcliente de último que haya en la base de datos y le suma 1
function getIdclienteNuevo() {
  console.log("HEMOS ENTRADO EN LA FUNCION getIdclienteNuevo()")
  var clienteMlab = requestJson.createClient(urlusuariosMlab + "?l=2000&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if(!err)
      {
        console.log("HEMOS ACCEDIDO A LA BBDD SIN ERROR.")
        var idcliente = Number(body.length) + 1
        console.log("1. Idclientenuevo = " + idcliente)
        return idcliente
      }
      else
      {
        console.log(body)
      }
    })
}

/**
 * @swagger
 * /api/altausuario:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para dar de alta un nuevo usuario en el sistema.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: usuario
 *         description: Objeto de tipo usuario (cliente)
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/datosusuarionuevo'
 *     responses:
 *       200:
 *         description: Mensaje que indica que el usuario se ha dado de alta correctamente.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       404:
 *         description: Un mensaje indicando que no se ha podido efectuar el alta del cliente.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       400:
 *         description: Un mensaje indicando que no se ha podido efectuar el alta de la cuenta.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.post('/api/altausuario', function(req, res){
  var data = {}
  var filter = 'l=2000'
  var clienteMlab = requestJson.createClient(urlusuariosMlab + "?l=2000&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if(!err)
    {
      data.idcliente = Number(body.length) + 1
      console.log("1.Idclientenuevo = " + data.idcliente)
      data.nombre = req.body.nombre
      data.apellido = req.body.apellido
      data.email = req.body.email
      data.password = req.body.password
      data.ciudad = req.body.ciudad
      data.pais = req.body.pais
      data.direccion = req.body.direccion
      console.log("2.Idcliente = " + data.idcliente)

      clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + apiKey)

      clienteMlab.post('', data, function(err, resM, body) {
        if (!err) {
              var hoy = new Date();
              var dd = hoy.getDate();
              var mm = hoy.getMonth()+1;
              var yyyy = hoy.getFullYear();

              if(dd<10) {
                  dd='0'+dd
              }

              if(mm<10) {
                  mm='0'+mm
              }

              hoy = dd+'/'+mm+'/'+yyyy;
              var alta = {}
              alta.idcliente = data.idcliente
              console.log("3.Idcliente en movimiento = "+ alta.idcliente)
              alta.iban = String(Math.floor((Math.random() * 9999999999999999) + 1))
              var movimiento = []
              var nuevoMovimiento = {}
              nuevoMovimiento.id = 1
              nuevoMovimiento.fecha = hoy
              nuevoMovimiento.importe = "0.0"
              nuevoMovimiento.saldo = "0.0"
              nuevoMovimiento.tipo = "INGRESO"
              movimiento.push(nuevoMovimiento)
              alta.movimientos = movimiento
              clienteMovimiento = requestJson.createClient(urlmovimientosMlab + "?" + apiKey)
              clienteMovimiento.post('', alta, function(err, resM, body) {
                if (!err) {
                    res.send("Alta efectuada correctamente.")
                  }
                  else {
                    res.status(400).send('No se ha podido realizar la inserción de la cuenta.')
                  }
              })
          }
          else {
            res.status(404).send('No se ha podido realizar la inserción del cliente.')
          }
      })

    }
    else
    {
      console.log(body)
      res.status(400).send('No se ha podido obtener un nuevo idcliente para el nuevo cliente.')
    }
  })
})

// Función para convertir un array de objetos en un string que poder insertar en mlab con JSON.Parse
function getStringFromArray(array) {
  var movimientosString = '['
  for (var i = 0; i < array.length; i++) {
    movimientosString += '{"id":' + array[i].id + ','
    movimientosString += '"fecha":"' + array[i].fecha + '",'
    movimientosString += '"importe":"' + array[i].importe + '",'
    movimientosString += '"saldo":"' + array[i].saldo + '",'
    movimientosString += '"tipo":"' + array[i].tipo + '"}'
    if (i != array.length-1) {
      movimientosString += ','
    }
  }
  movimientosString += ']'
  return movimientosString
}

/**
 * @swagger
 * /api/movimientos:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para insertar un movimiento en una cuenta determinada.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: movimiento
 *         description: Objeto de tipo usuario (cliente)
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/datosmovimiento'
 *     responses:
 *       200:
 *         description: Mensaje que indica que el movimiento se ha insertado correctamente.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       400:
 *         description: Un mensaje indicando que no se ha podido efectuar la insercción debido a un error inesperado.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       404:
 *         description: Un mensaje indicando que no se ha podido efectuar la insercción debido a que la cuenta no existe.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.post('/api/movimientos', function(req, res) {
  /* Crear movimiento en MLab */
  var data = req.body
  var iban = data.iban
  var fecha = data.fecha
  var importe = data.importe
  var tipo = data.tipo
  console.log("iban = " + iban)
  console.log("fecha = " + fecha)
  console.log("importe = " + importe)
  console.log("tipo = " + tipo)
  var query = 'q={"iban":"' + iban + '"}'
  var filter = 'f={"movimientos":1}'

  clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay cuentas
        var movimiento = {}
        var movimientos = body[0].movimientos
        var id = movimientos[movimientos.length-1].id+1
        movimiento.id = id
        movimiento.fecha = fecha
        movimiento.importe = importe
        movimiento.saldo = String(Number(movimientos[movimientos.length-1].saldo) + Number(importe))
        movimiento.tipo = tipo
        movimientos.push(movimiento)

        var cambio = '{"$set":{"movimientos":' + getStringFromArray(movimientos) + '}}'

        clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
          if (!errP) {
            res.send("Movimiento insertado correctamente")
          }
          else {
            res.status(400).send('Error al insertar el movimiento')
          }
        })
      }
      else {
        res.status(404).send('No existe ningún cliente con ese número de cuenta.')
      }
    }
  })
})

/**
 * @swagger
 * /api/saldo/{iban}/{salidaenjson}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada para obtener el saldo de una cuenta determianda.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: iban
 *         description: iban de la cuenta de la que queremos saber el saldo.
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *         example: NL51 TKMS 6419 1963 22
 *       - name: salidaenjson
 *         description: 1 si se desea que la salida sea en formado json, o 0 si se desea la salida como número entero.
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       200:
 *         description: Un objeto con el saldo de la cuenta.
 *         schema:
 *           $ref: '#/definitions/saldo'
 *       404:
 *         description: Mensaje indicando que no existe esa cuenta
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.get('/api/saldo/:iban/:json', function(req, res){
  var iban = req.params.iban
  var json = req.params.json
  var query = 'q={"iban":"' + iban + '"}'
  var filter = 'f={"movimientos":1}'
  clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0) { // Hay cuentas
        var movimientos = body[0].movimientos
        var saldo = movimientos[movimientos.length-1].saldo
        if (json == 1) {
          var saldoJson = {}
          saldoJson.saldo = saldo
          res.send(saldoJson)
        }
        else {
          res.send(saldo)
        }
      }
      else {
        res.status(404).send('No existe ninguna cuenta con ese identificador IBAN.')
      }
    }
  })
})

/**
 * @swagger
 * /api/cambiopassword:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para modificar la contraseña de un cliente.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: passwords
 *         description: Objeto con la información del cambio de contraseña
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/datospassword'
 *     responses:
 *       200:
 *         description: Mensaje que indica que el cambio de contraseña se ha realizado correctamente.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       404:
 *         description: Un mensaje indicando que no se ha podido efectuar el cambio de contraseña por haber introducido la contraseña actual incorrectamente o porque el usuario no existe.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.post('/api/cambiopassword', function(req, res){
var user = req.body
var idcliente = user.idcliente
var oldPassword = user.oldPassword
var newPassword = user.newPassword
var query = 'q={"idcliente":' + idcliente + ',"password":"' + oldPassword + '"}'
clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + query + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) {
      clienteMlab = requestJson.createClient(urlusuariosMlab)
      var cambio = '{"$set":{"password":"' + newPassword + '"}}'
      clienteMlab.put('?q={"idcliente": ' + idcliente + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
        res.send("La contraseña se ha modificado correctamente")
     })
    }
    else {
      res.status(404).send('Usuario no encontrado o contraseña incorrecta')
    }
  }
})
})

/**
 * @swagger
 * /api/graficos/{iban}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada para obtener una lista con la evolución del saldo de los movimientos de una cuenta con el objetivo de pintar una gráfica de evolución del saldo utilizando la libreria Javascript Google Chrts.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: iban
 *         description: iban de la cuenta deee la que queremos saber los movimientos
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *         example: NL51 TKMS 6419 1963 22
 *     responses:
 *       200:
 *         description: Un objeto con un array con los saldos de los últimos movimientos de la cuenta
 *         schema:
 *           $ref: '#/definitions/graficos'
 *       404:
 *         description: Mensaje indicando que no existe esa cuenta.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.get('/api/graficos/:iban', function(req, res){
var iban = req.params.iban
var query = 'q={"iban":"' + iban + '"}'
var filter = 'f={"movimientos":1,"_id":0}'

clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // Hay cuentas
      var movimientos = body[0].movimientos
      var jsonsaldos = {}
      var arraysaldos = []
      var ingresos = 0
      var gastos = 0
      var importe = 0
      for (var i = 0; i < movimientos.length; i++) {
        importe = Number(movimientos[i].importe)
        if (importe < 0) {
            gastos += importe
        }
        else {
          ingresos += importe
        }
        arraysaldos.push(Number(movimientos[i].saldo))
      }
      jsonsaldos.ingresos = ingresos
      jsonsaldos.gastos = gastos * -1
      jsonsaldos.saldos = arraysaldos
      res.send(jsonsaldos)
    }
    else {
      res.status(404).send('No existe ningún cliente con ese número de cuenta.')
    }
  }
})
})

/**
 * @swagger
 * /api/v2/graficos/{iban}:
 *   get:
 *     tags:
 *       - Api
 *     description: Llamada para obtener una lista con la evolución del saldo de los movimientos de una cuenta con el objetivo de pintar una gráfica de evolución del saldo utilizando la libreria Javascript Google Chrts.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: iban
 *         description: iban de la cuenta deee la que queremos saber los movimientos
 *         in: path
 *         required: true
 *         schema:
 *           type: string
 *         example: NL51 TKMS 6419 1963 22
 *     responses:
 *       200:
 *         description: Un objeto con un array con los saldos de los últimos movimientos de la cuenta
 *         schema:
 *           $ref: '#/definitions/graficosv2'
 *       404:
 *         description: Mensaje indicando que no existe esa cuenta.
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 */
app.get('/api/v2/graficos/:iban', function(req, res){
var iban = req.params.iban
var query = 'q={"iban":"' + iban + '"}'
var filter = 'f={"movimientos":1,"_id":0}'

clienteMlab = requestJson.createClient(urlmovimientosMlab + "?" + query + "&" + filter + "&" + apiKey)

clienteMlab.get('', function(err, resM, body) {
  if (!err) {
    if (body.length > 0) { // Hay cuentas
      var movimientos = body[0].movimientos
      var chart = {}
      var arraysaldos = []
      arraysaldos.push(['Fecha','Saldo'])
      var ingresos = 0
      var gastos = 0
      var importe = 0
      for (var i = 0; i < movimientos.length; i++) {
        importe = Number(movimientos[i].importe)
        if (importe < 0) {
            gastos += importe
        }
        else {
          ingresos += importe
        }
        arraysaldos.push([movimientos[i].fecha, Number(movimientos[i].saldo)])
      }

      chart.piechart = [['ingresos', ingresos], ['gastos', (gastos * -1)]]
      chart.saldos = arraysaldos
      res.send(chart)
    }
    else {
      res.status(404).send('No existe ningún cliente con ese número de cuenta.')
    }
  }
})
})

/**
 * @swagger
 * /api/modificarDatos:
 *   post:
 *     tags:
 *       - Api
 *     description: Llamada para realizar una modificación de datos de un cliente identificado por su {idcliente}.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: usuario
 *         description: Objeto de tipo usuario (cliente)
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/datosusuarioexistente'
 *     responses:
 *       200:
 *         description: Un mensaje indicando que la modificación se ha realizado correctamente.
 *       400:
 *         description: Un mensaje indicando que no se han podido modificar los datos por un error inesperado.
 *       404:
 *         description: Un mensaje indicando que no existe un usuario con ese identificar.
 */
app.post('/api/modificarDatos', function(req, res){
  var user = req.body
  var idcliente = user.idcliente
  var query = 'q={"idcliente":' + idcliente + '}'
  console.log("query = " + query)
  clienteMlab = requestJson.createClient(urlusuariosMlab + "?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      console.log("body.length = " + body.length)
      if (body.length > 0) {
        clienteMlab = requestJson.createClient(urlusuariosMlab)
        var cambio = '{"$set":' + JSON.stringify(user) + '}'
        console.log("cambio = " + cambio)
        clienteMlab.put('?q={"idcliente": ' + idcliente + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
          if (!errP) {
            res.status(200).send('Datos modificados correctamente.')
          }
          else{
            res.status(400).send('No se han mnodificado los datos debido a un error inesperado.')
          }
        })
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
