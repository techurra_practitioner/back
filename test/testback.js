var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')
var should = chai.should()
var url = 'http://127.0.0.1:3000'
chai.use(chaiHttp) // Configurando chai con módulo http

// Test 1: Probar si hay conectividad con MLab a través de a Internet
describe('Tests de conectividad', () => {
  it('Hay conectividad con mlab?', (done) => {
    chai.request(url)
        .get('/')
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
  })
})

// Test 2: Probar que la API está levantada y atendiendo peticiones
describe('Tests para verificar que la API está escuchando', () => {
  it('Hay conectividad con la API?', (done) => {
    chai.request(url)
        .get('/api')
        .end((err, res) => {
          if (err) return done(err);
          res.should.have.status(200)
          // res.body.mensaje.should.eql("Bienvenido a la API del proyecto Tech-U Practitioner Bootcamp del Red Ribon Army")
          // res.should.body.mensaje.eql("Bienvenido a la API del proyecto Tech-U Practitioner Bootcamp del Red Ribon Army")
          done()
        })
  })
})

// Test 3: Probar que funciona la llamada que devuelve los datos de un cliente
it('Test para verificar que funciona la llamada que devuelve los datos de un cliente', function(done) {
  chai.request(url)
      .get('/api/cliente/808')
      .end(function(err, res){
        should.equal(err, null)
        res.should.have.status(200)
        res.should.be.json
        res.body.should.be.a('object')
        res.body.should.have.property('_id')
        res.body.should.have.property('nombre')
        res.body.should.have.property('apellido')
        res.body.nombre.should.equal('Saleem')
        res.body.apellido.should.equal('Burrill')
        done()
      })
})

// Test 4: Probar que funciona la llamada que devuelve las cuentas de un cliente
it('Test para verificar que funciona la llamada que devuelve las cuentas de un cliente', function(done) {
  chai.request(url)
      .get('/api/cuentas/808')
      .end(function(err, res){
        should.equal(err, null)
        res.should.have.status(200)
        res.should.be.json
        res.body.should.be.a('array')
        res.body.should.have.length(2)
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('iban')
        res.body[0].iban.should.be.a('string')
        res.body[0].iban.should.equal('NL51 TKMS 6419 1963 22')
        res.body[1].should.be.a('object')
        res.body[1].should.have.property('iban')
        res.body[1].iban.should.be.a('string')
        res.body[1].iban.should.equal('FR85 6070 7305 09KN IAKF PRAV J25')
        done()
      })
})

// Test 5: Probar que funciona la llamada que devuelve los movimientos de una cuenta
it('Test para verificar que fuciona la llamada que devuelve los movimientos de una cuenta', function(done) {
  chai.request(url)
      .get('/api/movimientos/NL51 TKMS 6419 1963 22')
      .end(function(err, res){
        should.equal(err, null)
        res.should.have.status(200)
        res.should.be.json
        res.body.should.be.a('array')
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('fecha')
        res.body[0].should.have.property('importe')
        res.body[0].should.have.property('saldo')
        res.body[0].should.have.property('tipo')
        res.body[0].fecha.should.equal('01/01/2018')
        res.body[0].importe.should.equal('-1167.32')
        res.body[0].saldo.should.equal('8832.68')
        res.body[0].tipo.should.equal('REINTEGRO')
        done()
      })
})

// // Test 6: Probar que se se inicia sesión correctamente en ela apliación
// describe('Tests para verificar el inicio de sesión.', () => {
//   it('Inicio de sesión en la aplicación', (done) => {
//     chai.request(url)
//         .post('/api/login')
//         .send({'email': 'sburrillmf@ow.ly', 'password': '7QLKKCrvj89C'})
//         .end((err, res) => {
// //          if (err) return done(err);
//           res.should.have.status(200)
//           res.should.be.json
//           res.body.should.be.a('object')
//           res.body.should.have.property('login')
//           res.body.login.should.equal('ok')
//           done()
//         })
//   })
// })
